import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;


public class Malbury {

    public static void main(String[] args) {
        Person person1 = new Person("Nurlan",30,
                LocalDate.of(1990, Month.JUNE, 29),
                Person.Cins.MALE,"amrahovnurlan@gmail.com");
        Person person2 = new Person("Vasif",32,
                LocalDate.of(1990, Month.JUNE, 12),
                Person.Cins.MALE,"vasif@gmail.com");
        Person person3 = new Person("Nihat",15,
                LocalDate.of(2005, Month.JUNE, 1),
                Person.Cins.MALE,"nihat@gmail.com");
        Person person4 = new Person("Aygul",10,
                LocalDate.of(2010, Month.JUNE, 29),
                Person.Cins.FEMALE,"aygul@gmail.com");

        List<Person> roster = new ArrayList<>();
        roster.add(person1);
        roster.add(person2);
        roster.add(person3);
        roster.add(person4);


      /*  roster.stream()
                .filter(
                        p -> p.getGender() == Person.Cins.MALE &&
                             p.getAge() >= 18 &&
                                p.getAge()<=32
                )
                .map(Person::getEmailAddress)
                .forEach(System.out::println);*/

       // roster.forEach(System.out::println);

       // Collections.sort(roster, Comparator.comparing(Person::getName));

       /* roster.stream()
                .filter(p->p.getName().startsWith("N"))
                .forEach(x -> System.out.println(x.getName()));*/

/*
       long count = roster.stream()
                .filter(p->p.getName().startsWith("N"))
                .count();
        System.out.println(count);*/

       /* Map<String, String> map = new HashMap<String, String>();

        map.put("A", "Alex");
        map.put("B", "Brian");
        map.put("C", "Charles");

        map.forEach((k, v) ->
                System.out.println("Key = " + k + ", Value = " + v));*/


       /* List<Integer> numberList = Arrays.asList(1,2,3,4,5);
        numberList.stream()
                .filter(n -> n%2  == 0)
                .forEach(System.out::println);*/


       /* Stream<Integer> stream = Stream.of(1,2,3,4,5,6,7,8,9);
        stream.forEach(p -> System.out.println(p));*/

      /*  Stream<Integer> stream = Stream.of(new Integer[]{1,2,3,4,5,6,7,8,9});
        stream.forEach(p -> System.out.println(p));*/


      /*  List<Integer> list = new ArrayList<Integer>();

        for(int i = 1; i< 10; i++){
            list.add(i);
        }

        Stream<Integer> stream = list.stream();
        stream.forEach(p -> System.out.println(p));*/


       /* Stream<String> stream = Stream.of("A$B$C".split("\\$"));
        stream.forEach(p -> System.out.println(p));*/


      /*  List<Integer> list = new ArrayList<Integer>();
        for(int i = 1; i< 10; i++){
            list.add(i);
        }
        Stream<Integer> stream = list.stream();
        List<Integer> evenNumbersList = stream.filter(i -> i%2 == 0).collect(Collectors.toList());
        System.out.print(evenNumbersList);*/

    /*    List<Integer> list = new ArrayList<>();
        for(int i = 1; i< 10; i++){
            list.add(i);
        }
        Stream<Integer> stream = list.stream();
        Integer[] evenNumbersArr = stream.filter(i -> i%2 == 0).toArray(Integer[]::new);
        System.out.print(Arrays.toString(evenNumbersArr));*/


        List<String> memberNames = new ArrayList<>();
        memberNames.add("Amitabh");
        memberNames.add("Shekhar");
        memberNames.add("Aman");
        memberNames.add("Rahul");
        memberNames.add("Shahrukh");
        memberNames.add("Salman");
        memberNames.add("Yana");
        memberNames.add("Lokesh");


      /*  memberNames.stream().sorted()
                .forEach(System.out::println);*/

      /*  List<String> memNamesInUppercase = memberNames.stream().sorted()
                .map(String::toUpperCase)
                .collect(Collectors.toList());

        System.out.print(memNamesInUppercase); */


       /* Optional<String> reduced = memberNames.stream()
                .reduce((s1,s2) -> s1 + "#" + s2);

        reduced.ifPresent(System.out::println);*/


      /*  String firstMatchedName = memberNames.stream()
                .filter((s) -> s.startsWith("A"))
                .findFirst().get();

        System.out.println(firstMatchedName);*/


       /* List<Integer> integers = Arrays.asList(1,12,433,5);

        Optional<Integer> max = integers.stream().reduce(Math::max);

        max.ifPresent(System.out::println);*/


       /* List<String> strings = Arrays
                .asList("how", "to", "do", "in", "java", "dot", "com");

        List<String> sorted = strings
                .stream()
                .sorted(String::compareTo)
                .collect(Collectors.toList());

        System.out.println(sorted);*/


      /*  List<Integer> integers = IntStream
                .range(1, 100)
                .boxed()
                .collect(Collectors.toCollection( ArrayList::new ));

        Optional<Integer> max = integers.stream().reduce(Math::max);

        max.ifPresent(System.out::println);*/


     /*
        //Assume this value has returned from a method
        Optional<Person> personOptional = Optional.empty();

        //Now check optional; if value is present then return it,
        //else create a new Company object and retur it
        Person person = personOptional.orElse(new Person());

        //OR you can throw an exception as well
        Person person = personOptional.orElseThrow(IllegalStateException::new);*/

       /* String armisticeDate = "2016-04-04";

        LocalDate aLD = LocalDate.parse(armisticeDate);
        System.out.println("Date: " + aLD);*/


      /*  String anotherDate = "04 Apr 2016";

        DateTimeFormatter df = DateTimeFormatter.ofPattern("dd MMM yyyy");
        LocalDate random = LocalDate.parse(anotherDate, df);

        System.out.println(anotherDate + " parses as " + random);*/








    }






}
